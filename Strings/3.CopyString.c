//
// Created by naman on 02/03/22.
//
// 3) Using loop, copy a string into another string. Display the copied string using %s.

#include <stdio.h>

int main(){
    printf("Enter the string : ");
    char string1[100], string2[100];
    scanf("%s", string1);
    int i = 0;
    while (string1[i] != '\0') {
        string2[i] = string1[i];
        i++;
    }
    string2[i] = '\0';
    printf("Copied string is : %s ", string2);
}