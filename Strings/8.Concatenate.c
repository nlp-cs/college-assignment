//
// Created by naman on 04/03/22.
//
//8) Concatenate second string on the right of first string. First string should get changed and no third string should be used.

#include <stdio.h>

int main(){
    printf("Enter the first string : ");
    char string1[100];
    scanf("%s", string1);
    int i =0;
    while (string1[i] != '\0')
        i++;
    string1[i] = ' ';
    i++;
    char string2[100 - i];
    printf("Enter second string : ");
    scanf(" %s", string2);
    for (int j=0;  string2[j] != '\0'; j++){
        string1[i] =string2[j];
        i++;
    }
    printf("String is : %s", string1);
}