//
// Created by naman on 03/02/22.
//

//5) **Find whether a given integer number is palindrome or not e.g. 12321 is a palindrome, 1221 is a palindrome, but 12331 is not.

#include <stdio.h>

int main() {
    printf("Enter number : ");
    int originalNum=0;
    scanf("%d", &originalNum);
    int copyOriginal= originalNum, reverseNum=0;
    while (copyOriginal!=0){
        reverseNum= reverseNum*10 + copyOriginal%10;
        copyOriginal/=10;
    }

    if (originalNum==reverseNum)
        printf("Yes, Number is palindrome because");
    else
        printf("No, Number is not palindrome because");
    printf("reverse Number of %d is %d", originalNum, reverseNum);
}