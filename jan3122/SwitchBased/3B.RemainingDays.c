//
// Created by naman on 08/02/22.
//
//3) Read a date as collection of 3 integers (d,m,y). Using switch-case, find
//number of remaining days in that month. For example 15 Aug 2019
//means, 16 days remaining. Implement it using (i) calculating number of
//remaining days in all 12 different cases (ii) think of doing these
//calculations min time i.e. clubbing together same calculation cases and
//calculating once only for the clubbed cases

#include <stdio.h>

int main() {
    printf("Enter Date DD MM YYYY  :  ");
    int date = 0, month = 0, year = 0;
    scanf("%d %d %d",
          &date,
          &month,
          &year);

    //calculating number of remaining days in all 12 different cases

    switch (month) {
        case 1: case 3: case 5: case 7: case 8: case 10: case 12:
            printf("Remaining days in this month are %d", 31-date); break;
        case 2:
            if (year%4==0)
                printf("and %d are remaining in this month", 29-date);
            else
                printf("and %d are remaining in this month", 28-date);
            break;
        case 4: case 6: case 9: case 11:
            printf("Remaining days in this month are %d", 30-date); break;
        default:
            printf("Sorry, Invalid operator");
    }
}