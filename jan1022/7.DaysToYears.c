//
// Created by naman on 10/01/22.
//
// Question : Given numbers of days, Compute numbers of years, months & remaining days. Can you handle leap years as well?
//

#include <stdio.h>

int main(){
    int days = 0;
    printf("Enter number of days : ");
    scanf("%d", &days);
    int years = days / 365;
    int leapYears = days / 1460;
    days = days - leapYears - (years * 365);

    int months = 0;

    if (days >= 31){ // January
        months++;
        days -=31;
        if (years % 4 == 0 && days >=29){ //February
            months++;
            days -= 29;
        } else if (years % 4 !=0 && days >= 28){
            months++;
            days -= 28;
        }
    }

    if (days >= 31){ //March
        months ++;
        days -= 31;
        if (days >= 30){ //April
            months++;
            days-=30;
            if (days >= 31){ //May
                months++;
                days -= 31;
                if (days >= 30) { //June
                    months++;
                    days -= 30;
                    if (days >= 31){ //July
                        months++;
                        days -= 31;
                        if (days >= 31){ //August
                            months ++;
                            days -=31;
                            if (days >= 30){ //September
                                months++;
                                days -= 30;
                                if (days >= 31){ //October
                                    months ++;
                                    days -= 31;
                                    if (days >= 30) { //November
                                        months++;
                                        days -= 30;
                                        if (days>= 31){ //December
                                            months++;
                                            days -= 31;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    printf("\nThere are %d total years, out of which %d are leap years.\nThere are %d months and %d days left. ", years, leapYears, months, days);

    return 0;
}