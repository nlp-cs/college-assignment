//
// Created by Naman on 03/01/22.
//

#include <stdio.h>

int main(){
    printf("Input first number : ");
    float num1 = 0, num2 = 0;
    scanf("%f", &num1);
    printf("\nInput second number : ");
    scanf("%f", &num2);
    if (num1 == num2)
        printf("\nBoth numbers are equal and their value is :  %f " , num1);
    if (num1 > num2)
        printf("\nFirst number (%f) is greater than second number (%f) ", num1, num2);
    if (num1 < num2)
        printf("\nSecond number (%f) is greater than first number (%f) ", num2, num1);

    return 0;
}