//
// Created by naman on 03/01/22.
//

#include <stdio.h>

int main(){
    printf("Input first number : ");
    float num1 = 0, num2 = 0;
    scanf("%f", &num1);
    printf("\nInput second number : ");
    scanf("%f", &num2);
    if (num1 == num2)
        printf("\nBoth numbers are equal. No task performed.");
    if (num1 > num2)
        printf("\nFirst number (%f) is greater than second number (%f). \nSum of numbers is , %f ", num1, num2, num1 +num2);
    if (num1 < num2)
        printf("\nSecond number (%f) is greater than first number (%f). First number subtracted from second number gives : %f ", num2, num1, num2-num1);

    return 0;
}