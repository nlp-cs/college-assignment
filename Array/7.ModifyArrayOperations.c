//
// Created by naman on 21/02/22.
//
//7) Change the above program such that for n being odd, middle value is added with itself.
//This is to be done without using any if statement. Use only one for loop and don't use any if
//inside it. Same loop should work correctly for n being even as well as odd.

#include <stdio.h>

int main(){
    int num = 0;
    printf("Please Enter Number of elements : ");
    scanf("%d", &num);
    int array1[num];
    printf("Enter Array : ");
    for (int i = 0;i < num; ++i) {
        scanf(" %d", &array1[i]);
    }
    for (int i=0; i<=num/2; i++){
        printf("\nSum of pair is : %d", array1[i]+array1[num-i-1]);
    }
}