//
// Created by naman on 20/02/22.
//
//10) For a class of n students (roll number 1..N), store their physics, chem, math marks in 3
//separate 1-dimensional arrays. Find the total marks scored by the student having the highest marks in (i) physics (ii) chem (iii) math.Report if same student has the highest marks in more
//than one subject. Assume that there are no duplicity in the highest marks of a subject.

#include <stdio.h>
int main()
{
    int number;
    printf("Enter the number of students :\n");
    scanf("%d",&number);
    int marks[number+2][4];
    for(int i=0;i<number+2;i++){
        for (int j=0;j<4;j++) {
            marks[i][j] = 0;}
    }
    for(int student=1;student<=number;student++){
        printf("Enter the marks of Chemistry of Roll No. %d :  ",student);
        scanf(" %d",&marks[student][0]);
        printf("Enter the marks of Physics of Roll No. %d :  ",student);
        scanf(" %d",&marks[student][1]);
        printf("Enter the marks of Maths of Roll No. %d :  ",student);
        scanf(" %d",&marks[student][2]);
        for (int j=0;j<4;j++){
            if (marks[student][j]>marks[0][j]){
                marks[0][j] = marks[student][j];
                marks[number+1][j] = student;
            }}
        printf("----------------------------------------------------------------------------\n");
    }
    printf("Max marks in subject in subject of Chemistry is scored by Roll number %d.\nTotal marks of student : %d \n\n", marks[number+1][0], marks[marks[number+1][0]][0]+marks[marks[number+1][0]][1]+marks[marks[number+1][0]][2]);
    printf("Max marks in subject in subject of Physics is scored by Roll number %d.\nTotal marks of student : %d \n\n", marks[number+1][1], marks[marks[number+1][1]][0]+marks[marks[number+1][1]][1]+marks[marks[number+1][1]][2]);
    printf("Max marks in subject in subject of Maths is scored by Roll number %d.\nTotal marks of student : %d \n\n", marks[number+1][2], marks[marks[number+1][2]][0]+marks[marks[number+1][2]][1]+marks[marks[number+1][2]][2]);

    if (marks[number+1][0]==marks[number+1][1] || marks[number+1][0]==marks[number+1][2] )
        printf("Roll number %d has scored highest marks in more than one subject", marks[number+1][0]);
    else if (marks[number+1][1]==marks[number+1][2] )
        printf("Roll number %d has scored highest marks in more than one subject", marks[number+1][1]);
}