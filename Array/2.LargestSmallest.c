//
// Created by naman on 15/02/22.
//
//2) Find largest element in array, now find smallest element of the array.

#include <stdio.h>

int main(){
    int num = 0;
    printf("Please Enter Number of elements : ");
    scanf("%d", &num);
    int array1[num];
    printf("Enter Array : ");
    for (int i = 0;i < num; ++i) {
        scanf(" %d", &array1[i]);
    }

    int largest = array1[0], smallest = array1[0];
    for (int i = 1; i<num; i++){
        if (array1[i] > largest)
            largest= array1[i];
        if (array1[i] < smallest)
            smallest= array1[i];
    }
    printf("Largest value in Array is %d\nSmallest value in array is %d ", largest, smallest);
}