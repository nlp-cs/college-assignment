//
// Created by naman on 21/02/22.
//
// 8) Given an array, find index of first negative number. now find sum of all numbers from
//that index onwards (i) using break (ii) without using break, and using flag (iii) without break,
//without flag (hint: use loop from last backwards to find startindex)
#include <stdio.h>

int main(){
    int num = 0;
    printf("Please Enter Number of elements : ");
    scanf("%d", &num);
    int array1[num];
    printf("Enter Array : ");
    for (int i = 0;i < num; ++i) {
        scanf(" %d", &array1[i]);
    }
    int sum=0, flag=0;
    for (int i=0;i <num;i++){
        if (flag==1){
            sum+=array1[i];
        }
        if (array1[i]<0){
            flag = 1;
        }
    }
    printf("Sum from negative number onwards is : %d", sum);
}