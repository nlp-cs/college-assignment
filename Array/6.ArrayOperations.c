//
// Created by naman on 21/02/22.
//
//6) Input an array & find sum of first & last, 2nd& 2nd last term. If n is odd, print middle term alone.

#include <stdio.h>

int main(){
    int num = 0;
    printf("Please Enter Number of elements : ");
    scanf("%d", &num);
    int array1[num];
    printf("Enter Array : ");
    for (int i = 0;i < num; ++i) {
        scanf(" %d", &array1[i]);
    }
    printf("Sum of first and last term is : %d\nSum of 2nd and 2nd last terms is : %d", array1[0]+array1[num-1], array1[1]+array1[num-2]);
    if (num%2!=0){
        printf("\nMiddle number is : %d", array1[(num-1)/2]);
    }
}