//
// Created by naman on 28/01/22.
//
// 6) Find factorial of a number.

#include <stdio.h>

int main() {
    int totalNum = 0, fact=1;
    printf("Enter number to find factorial : ");
    scanf("%d", &totalNum);

    for (int i =1; i<= totalNum ; i++){
        fact*= i;
    }
    printf("Factorial of %d is %d", totalNum, fact);
}
