//
// Created by naman on 24/01/22.
//

//1) Using while, print Hello N times

#include <stdio.h>

int main(){
    int num = 0;
    printf("Enter number of times to print : ");
    scanf("%d", &num);
    while (num >0){
        printf("Hello ");
        num--;
    }
}