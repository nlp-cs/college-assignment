//
// Created by naman on 09/03/22.
//

#include <stdio.h>
#include <math.h>

double area(double height, double base, double angle){
    return  height * base * sin(angle) / 2;
}

int main(){
    int largestArea = 0;
    double plots[6][5]  = {
            {1, 137.4, 80.9, 0.78},
            { 2, 155.2, 92.62, 0.89 },
            {3, 149.3, 97.93, 1.35},
            { 4, 160.0, 100.25, 9.00},
            {5, 155.6, 68.95, 1.25},
            { 6, 149.7, 120.0, 1.75}
    };

    for (int i = 0; i <6; i++){
        plots[i][4] = area(plots[i][1], plots[i][2], plots[i][3]);
        printf("\n Area of Plot number %d is %f", (int)plots[i][0], plots[i][4] );
        if (plots[i][4] > plots[largestArea][4])
            largestArea = i;
    }

    printf("\n==================================================================\n");
    printf("\nPlot with largest are is plot number %d with total area %f", (int)plots[largestArea][0], plots[largestArea][4]);

}
