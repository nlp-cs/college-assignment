//
// Created by naman on 07/03/22.
//

//  Standard deviation of given data.

#include <stdio.h>
#include <math.h>

int main(){
   int givenArray[] = {-6, -12, 8,13,11,6,7,2 , -6, -9, -10, 11, 10, 9 , 2};
   double average = 0, variance = 0;
    printf("Given array is : ");
    for (int i=0; i<15;i++){
        printf(" %d ", givenArray[i]);
        average+= (double) givenArray[i];
    }
    average /= 15.0;
    for (int i=0; i<15; i++)
        variance +=  (givenArray[i] - average) *  (givenArray[i] - average)  / 15.0 ;
    printf("\nVariance of give data is : %f ", variance);

    double standardDeviation = sqrt(variance);
    printf("\nStandard deviation of give data is : %f ", standardDeviation);

}