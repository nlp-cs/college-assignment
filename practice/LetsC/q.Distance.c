//
// Created by naman on 09/03/22.
//

// Input 10 co-ordinates, find sum of distances of consecutive points.

#include <stdio.h>
#include <math.h>

int main(){
    double  distance = 0, x1=0, x2=0, y1=0, y2=0;
    printf("Enter the first X and Y coordinates : ");
    scanf(" %lf %lf", &x1, &y1);
    for (int i =0; i <8; i++){
        printf("Enter next X and Y coordinates : ");
        scanf(" %lf %lf", &x2, &y2);
        distance+= sqrt( (x1-x2)*(x1-x2) + (y1-y2)*(y1-y2));
        x1=x2; y1=y2;
    }
    printf("Enter the last X and Y coordinates : ");
    scanf(" %lf %lf", &x2, &y2);
    distance+= sqrt( (x1-x2)*(x1-x2) + (y1-y2)*(y1-y2));

    printf("The sum of distances between consecutive coordinates is : %lf", distance);

}